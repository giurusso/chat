package ch.heigvd.ad.tcp;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class ChatServer {

    static public void main(String args[]) throws IOException {
        if (args.length >= 2) {
            System.err.println("Too many parameters");
            System.exit(-1);
        }
        int port = args.length == 1 ? Integer.parseInt(args[0]) : ChatCommon.DEFAULT_PORT;

        ChatServer chatServer = new ChatServer(port);
        chatServer.run();
    }

    private ChatServer(int port) {
        this.port = port;
    }

    private final int port;

    private void run() throws IOException {

        ServerSocket serverSocket = new ServerSocket(port);

        while (serverSocket.isBound()) {
            Socket remoteSocket = serverSocket.accept();
            ServerThread serverThread =
                    new ServerThread(remoteSocket);
            serverThreads.add(serverThread);
            serverThread.start();
        }
    }

    synchronized private void broadcast(String msg, ServerThread except) {
        System.out.println(msg);
        for (ServerThread st : serverThreads)
            if (st != except)
                st.println(msg);
    }

    private Set<ServerThread> serverThreads = new HashSet<>();

    class ServerThread extends Thread {
        final PrintWriter printWriter;
        private final BufferedReader bufferedReader;
        private String nickname;

        String readLine() {
            try {
                return bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }


        void println(String line) {
            printWriter.print(line + "\n");
            printWriter.flush();
        }

        ServerThread(Socket socket) throws IOException {
            super();
            printWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }


        @Override
        public void run() {
            println("nickname? ");

            try {
                nickname = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            println("Welcome " + nickname);
            broadcast(nickname + " entered in the chat", this);

            String s;
            while ((s = readLine()) != null)
                broadcast(nickname + "> " + s, this);


            broadcast(nickname + " left in the chat", this);

            serverThreads.remove(this);
        }
    }
}
