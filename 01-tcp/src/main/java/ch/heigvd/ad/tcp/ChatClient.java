package ch.heigvd.ad.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ChatClient {
    static public void main(String args[]) throws IOException {
        if (args.length > 2) {
            System.err.println("Too many parameters");
            System.exit(-1);
        }
        int port = args.length == 2 ? Integer.parseInt(args[1]) : ChatCommon.DEFAULT_PORT;

        String host = args.length >= 1 ? args[0] : ChatCommon.DEFAULT_HOST;

        Socket socket = new Socket(host, port);

        Thread consoleToServerThread = new Thread(new StreamCopier(System.in, socket.getOutputStream()));
        consoleToServerThread.start();

        StreamCopier sc = (new StreamCopier(socket.getInputStream(), System.out));
        sc.run();

        System.in.close();
    }

}

class StreamCopier implements Runnable {
    @Override
    public void run() {
        int c;
        try {
            while ((c = is.read()) != -1) {
                os.write(c);
                if (c == '\n')
                    os.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    StreamCopier(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    private InputStream is;
    private OutputStream os;
}